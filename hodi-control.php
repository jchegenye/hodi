<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.carousel.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title>HODI | Device</title>
  </head>

    <body>

      <div class="suspended_icon">
        <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-contact-us.php">
          <img src="assets/for_a_demo.jpg" width="100%" class="img-fluid">
        </a>
      </div>

    <?php include('templates/partials/nav.php'); ?>

	<div class="container-fluid bg_header_img">
		<div class="row">

			<div class="container ">
				<div class="row">
					<div class="col-md-8 correct_margin">
						<div class="">
							<h3 class="title-blue">HODI Device</h3>
			                <h2 class="title-semi-big-slider title-small-top">Connections Management Platform</h2>
			                <p>Connect Reliably, Securely & Cost-efficiently</p>
						</div>
					</div>
					<div class="col-md-4">
						<!-- <img src="assets/solutions/unlimit-control/control_02_REVISED.gif" class="img-fluid header_overlay" alt="Header"> -->
					</div>
				</div>
			</div>
			

		</div>
	</div>

	<!-- <div class="container-fluid spacing-bg" id="verticles">
      <div class="container">
        <div class="row vertical-align">
          <div class="col-md-4 offset-md-4">
            <h2 class="text-center title-medium">VALUE PROPOSITION <div class="underline"></div></h2>
           </div>

        	<div class="col-md-12">
	            <p>Every enterprise has a unique model of using connected services to create new experiences, get closer to customers and grow the bottom line.</p>
            	<p>HODI Device can be customized to support any type of device deployment and network usage models so that you can tailor the implementation of device connectivity to your specific goals.</p>
          </div>
        </div>
      </div>
    </div> -->

	<div class="container spacing-bg">
		<div class="row vertical-align">
		  <div class="col-md-4 offset-md-4">
		    <h2 class="text-center title-medium">SOLUTION <div class="underline"></div></h2>
		   </div>

			<div class="col-md-12">
		        <p>Efficiently manage your entire enterprise centrally, control your device usage, reduce your costs, automate your operations and scale your business – these are the concerns that HODI Device can address. Our self service portal provides all of the tools to efficiently and profitably transform your enterprise into a connected device business.</p>
		  	</div>

		  	<div class="col-md-8 offset-md-2">
				<img src="assets/solutions/unlimit-control/control_final.png" class="img-fluid" alt="Header">
			</div>

		</div>
	</div>

  	<div class="container-fluid spacing-bg benefits" id="verticles">

        <div class="container">
          	<div class="row">
          		<div class="col-md-4 offset-md-4">
	            	<h2 class="text-center title-medium">BENEFITS <div class="underline"></div></h2>
	           	</div>
           	</div>

           	<div class="row">
	        	<div class="col-md-3">
		            <h6 class="text-left  title-medium"><img src="assets/solutions/unlimit-control/Flexibility-icon.png" class="img-fluid" alt="FLEXIBILITY"> FLEXIBILITY <div class="underline-2"></div></h6>
		            <ul class="list-inline">
		                <li class="title-small"><p><span class="ticks"></span> Adapted pricing: to suit needs of different industries, company sizes and businesses</p></li>
		                <li class="title-small"><p><span class="ticks"></span> Off-the-shelf packages are recommended for small and medium enterprises</p></li>
		                <li class="title-small"><p><span class="ticks"></span> Tailor-made plans are more suitable for larger accounts</p></li>
	              	</ul>
	          	</div>
	          	<div class="col-md-3">
		            <h6 class="text-left  title-medium"><img src="assets/solutions/unlimit-control/transparency-icon.png" class="img-fluid" alt="FLEXIBILITY"> TRANSPARENCY <div class="underline-2"></div></h6>
		            <ul class="list-inline">
		                <li class="title-small"><p><span class="ticks"></span> Complete dashboards give your business end-to-end oversight over your connected device operations</p></li>
		                <li class="title-small"><p><span class="ticks"></span> Try it today: Get instant, hands-on experience with the power of HODI Device</p></li>
	              	</ul>
	          	</div>
	          	<div class="col-md-3">
		            <h6 class="text-left  title-medium"><img src="assets/solutions/unlimit-control/custemer-icon.png" class="img-fluid" alt="FLEXIBILITY"> CUSTOMER EXPERIENCE <div class="underline-2"></div></h6>
		            <ul class="list-inline">
		                <li class="title-small"><p><span class="ticks"></span> HODI Device provides real-time connection, feeds, service diagnostics, and extraordinary automation options to enable unparalleled visibility and control</p></li>
	              	</ul>
	          	</div>
	          	<div class="col-md-3">
		            <h6 class="text-left  title-medium"><img src="assets/solutions/unlimit-control/support-icon.png" class="img-fluid" alt="FLEXIBILITY"> SUPPORT <div class="underline-2"></div></h6>
		            <ul class="list-inline">
		                <li class="title-small"><p><span class="ticks"></span>  24/7 technical support: Full support available for customers on top of self-management functionalities provided by the platform</p></li>
	              	</ul>
	          	</div>
	        </div>
       	</div>

      </div>
	</div>

	<div class="container spacing-bg">
      <div class="row">
        <div class="col-md-4 offset-md-4">
          <h2 class="text-center title-medium title-blue">GET STARTED <div class="underline"></div></h2>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4">
          <!-- <img src="assets/Unlimit-Control_new.gif.gif" class="img-fluid"> -->
        </div>

        <div class="col-md-8 emphasize-center">
          <h3 class="title-blue">HODI Device - STATER KIT</h3>

            <h6 class="title-small-top">Get instant, hands-on experience with the power of HODI Device Order.</h6>
            <p>This kit gives you the visibility to analyze and strategically manage your connected devices to grow and improve your business.</p>
            <p>The Starter Kit provides you with the following:</p>
            <ul>
            	<li>3 months of full access to HODI Device</li>
            	<li>3 test SIM cards – each with 100 MB and 50 SMS per month</li>
            </ul>

            <button class="btn btn-primary">REQUEST KIT</button>
        </div>
      </div>

    </div>

    <?php include('templates/partials/footer.php'); ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script> -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="OwlCarousel2-2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="main.js"></script>
    
  </body>
</html>