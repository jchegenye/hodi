<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.carousel.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title>HODI | Platform</title>
    
  </head>

    <body>

      <div class="suspended_icon">
        <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-contact-us.php">
          <img src="assets/for_a_demo.jpg" width="100%" class="img-fluid">
        </a>
      </div>

    <?php include('templates/partials/nav.php'); ?>

  		<div class="container-fluid bg_header_img">
  			<div class="row">

  				<div class="container ">
  					<div class="row">
  						<div class="col-md-8 correct_margin">
  							<div class="">
  								<h3 class="title-blue">HODI Platform</h3>
  				                <h2 class="title-semi-big-slider title-small-top">Device & Application Management Platform</h2>
  				                <p>Build, Develop and Deploy Smart Connected Solutions</p>
  							</div>
  						</div>
  						<div class="col-md-4">
  							<!-- <img src="assets/solutions/unlimit-enablement/enablement_02_revised-1.gif" class="img-fluid header_overlay" alt="Header"> -->
  						</div>
  					</div>
  				</div>
  				
  			</div>
  		</div>

  		<!-- <div class="container-fluid spacing-bg" id="verticles">
        <div class="container">
          <div class="row vertical-align">
            <div class="col-md-4 offset-md-4">
              <h2 class="text-center title-medium">VALUE PROPOSITION <div class="underline"></div></h2>
             </div>

          	<div class="col-md-12">
  	            <p>In the IoT space, connecting and managing devices is usually a time consuming and a challenging process. With HODI Platform, you can easily connect any device over any network. With our solution, you can start your IoT project today since we have over 100 certified and pre-integrated devices.</p>
            </div>
          </div>
        </div>
      </div> -->

	    <div class="container spacing-bg">
  			<div class="row vertical-align">
  			  <div class="col-md-4 offset-md-4">
  			    <h2 class="text-center title-medium">SOLUTION <div class="underline"></div></h2>
  			   </div>

  				<div class="col-md-12">
  			        <p>HODI Platform allows you to prove your end-to-end business concept of your connected devices and thus rapidly scale your deployment. Our platform enables data to be processed in real-time, based on your real world business rules, configurable in minutes.</p>
  			  	</div>

  			  	<div class="col-md-8 offset-md-2">
  					<img src="assets/solutions/unlimit-enablement/Enable_final.png" class="img-fluid" alt="Header">
  				</div>

  			</div>
  		</div>

  		<div class="container-fluid spacing-bg benefits" id="verticles">

  	        <div class="container">
  	          	<div class="row">
  	          		<div class="col-md-4 offset-md-4">
  		            	<h2 class="text-center title-medium">BENEFITS <div class="underline"></div></h2>
  		           	</div>
  	           	</div>

  	           	<div class="row">
  		        	<div class="col-md-3">
  			            <h6 class="text-left  title-medium"><img src="assets/solutions/unlimit-enablement/Customizability-icon.png" class="img-fluid" alt="FLEXIBILITY"> FLEXIBILITY <div class="underline-2"></div></h6>
  			            <ul class="list-inline">
  			                <li class="title-small"><p><span class="ticks"></span> Graphically assemble your own applications from a range of plugins with HODI Platform Application Builder</p></li>
  			                <li class="title-small"><p><span class="ticks"></span> Use the point-and-click interface of Rule Engine to create operationally relevant smart rules like – Send me an email if a machine is down for 60 minutes</p></li>
  		              	</ul>
  		          	</div>
  		          	<div class="col-md-3">
  			            <h6 class="text-left  title-medium"><img src="assets/solutions/unlimit-enablement/Scalbility-icon.png" class="img-fluid" alt="FLEXIBILITY"> TRANSPARENCY <div class="underline-2"></div></h6>
  			            <ul class="list-inline">
  			                <li class="title-small"><p><span class="ticks"></span> Highly scalable cloud, on-premise and hybrid infrastructure</p></li>
  			                <li class="title-small"><p><span class="ticks"></span> Highly secure and encrypted device communication</p></li>
  		              	</ul>
  		          	</div>
  		          	<div class="col-md-3">
  			            <h6 class="text-left  title-medium"><img src="assets/solutions/unlimit-enablement/Custmer-icon.png" class="img-fluid" alt="FLEXIBILITY"> CUSTOMER EXPERIENCE <div class="underline-2"></div></h6>
  			            <ul class="list-inline">
  			                <li class="title-small"><p><span class="ticks"></span> Use advanced dashboards and visualization capabilities with the Cockpit App to create customized user interfaces for different needs</p></li>
  			                <li class="title-small"><p><span class="ticks"></span> No development needed, just configure and use</p></li>
  		              	</ul>
  		          	</div>
  		          	<div class="col-md-3">
  			            <h6 class="text-left  title-medium"><img src="assets/solutions/unlimit-enablement/Support-icon.png" class="img-fluid" alt="FLEXIBILITY"> SUPPORT <div class="underline-2"></div></h6>
  			            <ul class="list-inline">
  			                <li class="title-small"><p><span class="ticks"></span>  24/7 Technical Support available for customers on top of self-management functionalities provided by the platform</p></li>
  		              	</ul>
  		          	</div>
  		        </div>
  	       	</div>
    	</div>

    <?php include('templates/partials/footer.php'); ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script> -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="OwlCarousel2-2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="main.js"></script>
    
  </body>
</html>