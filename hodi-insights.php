<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.carousel.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title>HODI | IOTA</title>
  </head>

    <body>

      <div class="suspended_icon">
        <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-contact-us.php">
          <img src="assets/for_a_demo.jpg" width="100%" class="img-fluid">
        </a>
      </div>

    <?php include('templates/partials/nav.php'); ?>

			<div class="container-fluid bg_header_img">
				<div class="row">

					<div class="container ">
						<div class="row">
							<div class="col-md-8 correct_margin">
								<div class="">
									<h3 class="title-blue">HODI IOTA</h3>
					                <h2 class="title-semi-big-slider title-small-top">Big Data, Analytics, AI & Cognitive Learning Platform</h2>
					                <p>Validate, Forecast & Optimize Your Business</p>
								</div>
							</div>
							<div class="col-md-4">
								<!-- <img src="assets/solutions/unlimit-insight/insight_02_revised.gif" class="img-fluid header_overlay" alt="Header"> -->
							</div>
						</div>
					</div>
					
				</div>
			</div>

			<!-- <div class="container-fluid spacing-bg" id="verticles">
		      <div class="container">
		        <div class="row vertical-align">
		          <div class="col-md-4 offset-md-4">
		            <h2 class="text-center title-medium">VALUE PROPOSITION <div class="underline"></div></h2>
		           </div>

		        	<div class="col-md-12">
			            <p>The last year has shown that artificial intelligence is here to stay. It will inevitably disrupt virtually each and every business model. With HODI Insights, you can explore the potential of Machine Learning, Cognitive Computing and Artificial Intelligence.</p>
		          </div>
		        </div>
		      </div>
		    </div> -->

		    <div class="container spacing-bg">
				<div class="row vertical-align">
				  <div class="col-md-4 offset-md-4">
				    <h2 class="text-center title-medium">SOLUTION <div class="underline"></div></h2>
				   </div>

					<div class="col-md-12">
				        <p class="text-center">HODI Insights allows you to anticipate & pre-empt disruptions, detect liabilities & mitigate risk and easily transform your business.</p>
				  	</div>

				  	<div class="col-md-10 offset-md-1">
						<img src="assets/solutions/unlimit-insight/Unlimit-Insights-soln.png" class="img-fluid" alt="Header">
					</div>

				</div>
			</div>

			<div class="container spacing-bg">
				<div class="row">
					<div class="col-md-4">
						<div class="soln_box1">
							<h3>Artificial Intelligence</h3>
							<p>Range of techniques including natural language processing, knowledge, reasoning and planning, for advanced tasks.</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="soln_box2">
							<h3>Cognitive Computing</h3>
							<p>Interactive decision-making and reasoning over deep domain models and evidence- based explanations, using Artificial intelligence/Machine Learning tools.</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="soln_box3">
							<h3>Machine Learning</h3>
							<p>Statistical analysis for pattern recognition to make data-driven predictions.</p>
						</div>
					</div>
				</div>
			</div>


			<div class="container-fluid spacing-bg benefits" id="verticles">

		        <div class="container">
		          	<div class="row">
		          		<div class="col-md-4 offset-md-4">
			            	<h2 class="text-center title-medium">BENEFITS <div class="underline"></div></h2>
			           	</div>
		           	</div>

		           	<div class="row">
			        	<div class="col-md-3">
				            <h6 class="text-left  title-medium"><img src="assets/solutions/unlimit-insight/Monetizability-icon.png" class="img-fluid" alt="MONETIZABILITY"> MONETIZABILITY <div class="underline-2"></div></h6>
				            <ul class="list-inline">
				                <li class="title-small"><p><span class="ticks"></span> Redefine business models based on the insights</p></li>
				                <li class="title-small"><p><span class="ticks"></span> Monetize data through asset-based online marketplaces</p></li>
				                <li class="title-small"><p><span class="ticks"></span> Garner new revenue from existing products and services</p></li>
			              	</ul>
			          	</div>
			          	<div class="col-md-3">
				            <h6 class="text-left  title-medium"><img src="assets/solutions/unlimit-insight/Operational-icon.png" class="img-fluid" alt="OPERATIONAL EXCELLENCE"> OPERATIONAL EXCELLENCE <div class="underline-2"></div></h6>
				            <ul class="list-inline">
				                <li class="title-small"><p><span class="ticks"></span> Gain new levels of visibility</p></li>
				                <li class="title-small"><p><span class="ticks"></span> Identify bottlenecks and inefﬁciencies to spotlight hidden costs</p></li>
				                <li class="title-small"><p><span class="ticks"></span>Optimize performance, empower employees and reduce costs</p></li>
			              	</ul>
			          	</div>
			          	<div class="col-md-3">
				            <h6 class="text-left  title-medium"><img src="assets/solutions/unlimit-insight/Custmer-icon.png" class="img-fluid" alt="FLEXIBILITY"> CUSTOMER EXPERIENCE <div class="underline-2"></div></h6>
				            <ul class="list-inline">
				                <li class="title-small"><p><span class="ticks"></span> Achieve new levels of customer engagement by gaining feedback throughout the product lifecycle</p></li>
			              	</ul>
			          	</div>
			          	<div class="col-md-3">
				            <h6 class="text-left  title-medium"><img src="assets/solutions/unlimit-insight/Support-icon.png" class="img-fluid" alt="FLEXIBILITY"> SUPPORT <div class="underline-2"></div></h6>
				            <ul class="list-inline">
				                <li class="title-small"><p><span class="ticks"></span>  24/7 Technical Support available for customers on top of self-management functionalities provided by the platform</p></li>
			              	</ul>
			          	</div>
			        </div>
		       	</div>

	      	</div>


    <?php include('templates/partials/footer.php'); ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script> -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="OwlCarousel2-2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="main.js"></script>
    
  </body>
</html>