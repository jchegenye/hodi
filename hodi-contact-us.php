  <!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.carousel.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title>HODI | Contact Us</title>
  </head>

    <body>

      <div class="suspended_icon">
        <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-contact-us.php">
          <img src="assets/for_a_demo.jpg" width="100%" class="img-fluid">
        </a>
      </div>

    <?php include('templates/partials/nav.php'); ?>

    <div class="container-fluid bg_header_img verticle">
      <div class="row">

        <div class="container ">
          <div class="row">
            <div class="col-md-8 correct_margin ">
              <div class="">
                  <h2 class="title-semi-big-slider title-small-top">Connecting Is Everything.</h2>
                  <p>Get In Touch With Us.</p>
              </div>
            </div>
            <div class="col-md-4">
              
            </div>
          </div>
        </div>
        
      </div>
    </div>

    <div class="container spacing-bg">
      <div class="row">
        <div class="col-md-4 offset-md-4">
          <h2 class="text-center title-medium title-blue">CONTACT US<div class="underline"></div></h2>
         </div>
      </div>

      <div class="row">
        <div class="col-md-4">
          <h4 class="title-blue">Corporate Office</h4>
          <address>
            P.O Box 12907,<br>
            Muthithi Rd, Nairobi,<br>
            Kenya.<br>

            Phone No: +254 20 3742821 <br>
              +254 722570112<br>
            Email: admin@alandick.co.ke
          </address>

          <iframe style="border: 1px solid #00bbdc; margin-top: 15px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.8338953994166!2d36.81104551529386!3d-1.272803335971939!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f1730235b9175%3A0xccf01295cf0b4839!2sAlanDick+(East+Africa)+Ltd!5e0!3m2!1sen!2ske!4v1503305982716" width="100%" height="260" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
          
          <h4 class="title-blue" style="margin-top: 15px;">Social Media Links</h4>

          <p>
            <a href="#" target="_blank" rel="noopener"><i class="fa fa-facebook fac"></i> /Hodi</a><br>
            <a href="#" target="_blank" rel="noopener"><i class="fa fa-twitter twi"></i> /Hodi</a><br>
            <a href="#" target="_blank" rel="noopener"><i class="fa fa-linkedin lin"></i> /Hodi</a>
          </p>
        </div>

        <div class="col-md-8">
          <h4 class="title-blue">General Inquiry</h4>

          <form>
            <p>All form fields are required.</p>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputEmail4">Name:*</label>
                <input type="email" class="form-control" id="inputEmail4">
              </div>
              <div class="form-group col-md-6">
                <label for="inputEmail4">Business E-Mail:*</label>
                <input type="email" class="form-control" id="inputEmail4">
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputEmail4">Phone Number:*</label>
                <input type="email" class="form-control" id="inputEmail4">
              </div>
              <div class="form-group col-md-6">
                <label for="inputEmail4">Company:*</label>
                <input type="email" class="form-control" id="inputEmail4">
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputEmail4">Phone Number:*</label>
                <textarea class="form-control" ></textarea>
              </div>
              <div class="form-group col-md-6"></div>
            </div>

            <div class="row">
              <div class="col-md-12 ">
                <button type="submit" class="btn btn-primary mx-auto d-block">Submit</button>
              </div>
            </div>

        </form>

        </div>
      </div>

    </div>

    <?php include('templates/partials/footer.php'); ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script> -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="OwlCarousel2-2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="main.js"></script>
    
  </body>
</html>