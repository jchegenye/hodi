<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.carousel.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title>HODI | Partners</title>
  </head>

    <body>

      <div class="suspended_icon">
        <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-contact-us.php">
          <img src="assets/for_a_demo.jpg" width="100%" class="img-fluid">
        </a>
      </div>

    <?php include('templates/partials/nav.php'); ?>

    <div class="container-fluid bg_header_img verticle">
      <div class="row">

        <div class="container ">
          <div class="row">
            <div class="col-md-8 correct_margin ">
              <div class="">
                  <h2 class="title-semi-big-slider title-small-top title-blue">PARTNERS</h2>
                  <p>Our Excellent Partner Ecosystem Encompasses Leading Device Manufacturers, Application Developers and Field Service Partners Among Others.</p>
              </div>
            </div>
            <div class="col-md-4">
              
            </div>
          </div>
        </div>
        
      </div>
    </div>

    <div class="container-fluid spacing-bg" id="featuredpartner">
      <div class="container">
        <div class="row">
          <div class="col-md-4 offset-md-4">
            <h2 class="text-center title-medium">FEATURED PARTNER<div class="underline"></div></h2>
           </div>
        </div>

        <div class="row">
          <div class="col-md-4">
            <img src="assets/partners/partner_side_image.png" class="img-fluid" style="width: 100%;">
          </div>

          <div class="col-md-8 emphasize-center">
            <h4>ROAMBEE</h4>
            <h5 class="title-small-top">Partner for Smart Logistics</h5>
            <p>The Roambee Corporation is an IoT supply chain and enterprise asset visibility company. They are pushing the boundaries on real time visibility of assets and goods outside the four walls of global enterprise with patented hardware and software technology combined with an array of sensor data, proprietary analytics, predictive reporting, and open APIs. Ultimately, Roambee’s IoT solutions will address a broad portfolio of enterprise challenges – not only for supply chain and in-field asset monitoring – but anywhere lack of real-time visibility creates inefficiencies and risk.</p>
          </div>
        </div>

      </div>
    </div>

    <!-- <div class="container spacing-bg">
      <div class="row vertical-align">
        <div class="col-md-4 offset-md-4">
          <h2 class="text-center title-medium title-blue">PARTNER CATALOGUE<div class="underline"></div></h2>
        </div>
      </div>

      <div class="row title-medium">
        <div class="col-md-2">
          <img src="assets/partners/roambee_first.png" class="img-fluid" alt="Header">
        </div>
        <div class="col-md-2">
          <img src="assets/partners/iTriangle_second.png" class="img-fluid" alt="Header">
        </div>
        <div class="col-md-2">
          <img src="assets/partners/Enrich_3.png" class="img-fluid" alt="Header">
        </div>
        <div class="col-md-2">
          <img src="assets/partners/fibcom_4.png" class="img-fluid" alt="Header">
        </div>
        <div class="col-md-2">
          <img src="assets/partners/cubastion-consulting_owler_20160301_122253_original_20.png" class="img-fluid" alt="Header">
        </div>
        <div class="col-md-2">
          <img src="assets/partners/Allied-Digital_6.png" class="img-fluid" alt="Header">
        </div>
      </div>

      <div class="row title-medium">
        <div class="col-md-2">
          <img src="assets/partners/Unified-Global-Tech_7.png" class="img-fluid" alt="Header">
        </div>
        <div class="col-md-2">
          <img src="assets/partners/Dynalog-India-Logo-EPS_8.png" class="img-fluid" alt="Header">
        </div>
        <div class="col-md-2">
          <img src="assets/partners/logo_9.png" class="img-fluid" alt="Header">
        </div>
        <div class="col-md-2">
          <img src="assets/partners/Avnet_logo_tagline_rgb_10.png" class="img-fluid" alt="Header">
        </div>
        <div class="col-md-2">
          <img src="assets/partners/CarSense-Logo_11.png" class="img-fluid" alt="Header">
        </div>
        <div class="col-md-2">
          <img src="assets/partners/Hyperthings_12.png" class="img-fluid" alt="Header">
        </div>
      </div>

      <div class="row title-medium">
        <div class="col-md-2">
          <img src="assets/partners/Anstel_13.png" class="img-fluid" alt="Header">
        </div>
        <div class="col-md-2">
          <img src="assets/partners/G-Trac-India-Mergers_14.png" class="img-fluid" alt="Header">
        </div>
        <div class="col-md-2">
          <img src="assets/partners/advantech-logo_16.png" class="img-fluid" alt="Header">
        </div>
        <div class="col-md-2">
          <img src="assets/partners/id-logo_17.png" class="img-fluid" alt="Header">
        </div>
        <div class="col-md-2">
          <img src="assets/partners/Azuga-Telematics.png" class="img-fluid" alt="Header">
        </div>
        <div class="col-md-2">
          <img src="assets/partners/Maestro_18.png" class="img-fluid" alt="Header">
        </div>
      </div>

      <div class="row">
        <div class="col-md-6 offset-md-4"><div class="row">
          <div class="col-md-4">
            <img src="assets/partners/pulsar-electronics-private-limited-logo-120x120_19.png" class="img-fluid" alt="Header">
          </div>
          <div class="col-md-4">
            <img src="assets/partners/pointer_21-1.png" class="img-fluid" alt="Header">
          </div></div> 
        </div>
      </div>

    </div> -->

    <div class="container-fluid spacing-bg benefits" id="verticles">
      <div class="container">
        <div class="row">
          <div class="col-md-4 offset-md-4">
            <h2 class="text-center title-medium">WHAT’S IN IT FOR YOU <div class="underline"></div></h2>
          </div>
        </div>

        <div class="row spacing-bg">
          <div class="col-md-4">
            <h5 class="text-left  title-medium"> Project Support<div class="underline-2"></div></h5>
            <ul class="list-inline">
              <li class="title-small"><p><span class="ticks"></span> Business and technical consultancy</p></li>
              <li class="title-small"><p><span class="ticks"></span> Ready to use application components – Platforms</p></li>
              <li class="title-small"><p><span class="ticks"></span> End to end project delivery support via relevant teams</p></li>
              <li class="title-small"><p><span class="ticks"></span> On-site support if required</p></li>
            </ul>
          </div>
          <div class="col-md-4">
            <h5 class="text-left  title-medium">Compatibility & Onboarding <div class="underline-2"></div></h5>
            <ul class="list-inline">
                <li class="title-small"><p><span class="ticks"></span> Product integration and Device-to-Platform onboarding of hardware partners</p></li>
                <li class="title-small"><p><span class="ticks"></span> Certification for product compatibility and onboarding for Unlimit Control</p></li>
                <li class="title-small"><p><span class="ticks"></span> Certification for product compatibility and onboarding for Unlimit Enablement</p></li>
                <li class="title-small"><p><span class="ticks"></span> Certification for product compatibility and onboarding for Unlimit Insights</p></li>
              </ul>
          </div>
          <div class="col-md-4">
            <h5 class="text-left  title-medium"> Marketing Support<div class="underline-2"></div></h5>
            <ul class="list-inline">
                <li class="title-small"><p><span class="ticks"></span> Joint press releases</p></li>
                <li class="title-small"><p><span class="ticks"></span> Web page listing</p></li>
                <li class="title-small"><p><span class="ticks"></span> Social media marketing and webinars</p></li>
                <li class="title-small"><p><span class="ticks"></span> Joint GTM and active co-promotion in client cases</p></li>
              </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid spacing-bg" id="join_us">
      <div class="container">
        <div class="row">
          <div class="col-md-4 offset-md-4">
            <h2 class="text-center title-medium title-blue">JOIN US<div class="underline"></div></h2>
          </div>
        </div>

        <form>

          <div class="form-row">

            <div class="form-group col-md-4">
              <label for="">Organisation Name:*</label>
              <input type="text" name="p_organ_name" class="form-control" id="">
            </div>

            <div class="form-group col-md-4">
              <label for="">Industry:*</label>
              <select class="form-control" name="p_industry" id="">
                <option selected>---</option>
                <option>Energy & Utilities</option>
                <option>Connected Cars</option>
                <option>Transport & Logistics</option>
                <option>Industry 4.0</option>
                <option>Agriculture</option>
                <option>Renewable Energy</option>
                <option>Banking & Financial Services</option>
                <option>Retail</option>
                <option>Healthcare</option>
              </select>
            </div>
            <div class="form-group col-md-4">
              <label for="">Partner Domain:*</label>
              <select class="form-control" name="p_domain" id="">
                <option selected>---</option>
                <option>OEM-Devices</option>
                <option>System Integrator</option>
                <option>Field Services</option>
                <option>Software & Application Development</option>
                <option>OEM-Sensors</option>
                <option>Consultancy</option>
                <option>Analytics</option>
                <option>Hardware Design House</option>
                <option>Healthcare</option>
                <option>Hardware Manufacturer</option>
                <option>Blockchain</option>
                <option>Artificial Intelligence</option>
              </select>
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="">Product/Solution Portfolio:*  <small class="">doc|pdf|txt|docx|rtf files only.</small></label>
              <input type="file" class="form-control-file" name="p_profile" id="">
            </div>

            <div class="form-group col-md-4">
              <label for="">Principal Place of Business:</label>
              <div class="row">
                <div class="col-md-6">
                  <input type="text" class="form-control" name="p_place_city" id="" placeholder="City"> 
                </div>
                <div class="col-md-6">
                  <input type="text" class="form-control" name="p_place_country" id="" placeholder="Country">
                </div>
              </div>
            </div>

            <div class="form-group col-md-4">
              <label for="">Headquarters:</label>
              <div class="row">
                <div class="col-md-6">
                  <input type="text" class="form-control" name="p_hq_city" id="" placeholder="City"> 
                </div>
                <div class="col-md-6">
                  <input type="text" class="form-control" name="p_hq_country" id="" placeholder="Country">
                </div>
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="">Website URL:</label>
              <input type="url" name="p_web_url" class="form-control" id="">
            </div>
            <div class="form-group col-md-4">
              <label for="">Year of Establishment:</label>
              <input type="text" name="p_year_esta" class="form-control" id="">
            </div>
            <div class="form-group col-md-4">
              <label for="">Revenue (in USD):</label>
              <input type="text" name="p_revenue" class="form-control" id="">
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="">Employee Base:</label>
              <input type="url" name="p_emply_base" class="form-control" id="">
            </div>
            <div class="form-group col-md-4">
              <label for="">Contact Person Name:*</label>
              <input type="text" name="p_contact_person" class="form-control" id="">
            </div>
            <div class="form-group col-md-4">
              <label for="">Official Designation:*</label>
              <input type="text" name="p_designation" class="form-control" id="">
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="">Mobile Number:*</label>
              <input type="number" name="p_mobile_number" class="form-control" id="">
            </div>
            <div class="form-group col-md-4">
              <label for="">Landline Number:</label>
              <input type="number" name="p_landline_no" class="form-control" id="">
            </div>
            <div class="form-group col-md-4">
              <label for="">E-mail ID:*</label>
              <input type="email" name="p_email" class="form-control" id="">
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-8">
              <label for="">Message:*</label>
              <textarea name="p_message" class="form-control"> </textarea>
            </div>
          </div>

          <button type="submit" class="btn btn-primary">Submit</button>

        </form>

      </div>
    </div>

    <?php include('templates/partials/footer.php'); ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script> -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="OwlCarousel2-2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="main.js"></script>
    
  </body>
</html>