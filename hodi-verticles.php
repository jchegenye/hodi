<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.carousel.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title>HODI | Verticles</title>
  </head>

    <body>

      <div class="suspended_icon">
        <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-contact-us.php">
          <img src="assets/for_a_demo.jpg" width="100%" class="img-fluid">
        </a>
      </div>

    <?php include('templates/partials/nav.php'); ?>

      <div class="container-fluid bg_header_img verticle">
        <div class="row">

          <div class="container ">
            <div class="row">
              <div class="col-md-8 correct_margin ">
                <div class="">
                    <h2 class="title-semi-big-slider title-small-top">Turn your IoT possibilities into reality</h2>
                    <p>Validate, Forecast & Optimize Your Business</p>
                </div>
              </div>
              <div class="col-md-4">
                
              </div>
            </div>
          </div>
          
        </div>
      </div>

      <div class="container-fluid sticky-top spacing-bg" id="verticles">
        <div class="container v_mainpage">
          <div class="row">
            <div class="col-md-4 offset-md-4">
              <h2 class="text-center title-medium">VERTICALS <div class="underline"></div></h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2">
              <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-verticles.php#connecte_cars">
                <div class="icon_box">
                  <img src="assets/Automotive.png" class="img-fluid" style="width: 100%;">
                  <h6>CONNECTED CARS</h6>
                </div>
              </a>
            </div>
            <!-- <div class="col-md-2">
              <a href="< ?php echo dirname($_SERVER['PHP_SELF']); ? >/hodi-verticles.php#transort_logistics">
                <div class="icon_box">
                  <img src="assets/TL.png" class="img-fluid" style="width: 100%;">
                  <h6>TRANSPORT & LOGISTICS</h6>
                </div>
              </a>
            </div> -->
            <div class="col-md-2">
              <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-verticles.php#industry_04">
                <div class="icon_box">
                  <img src="assets/Industry.png" class="img-fluid" style="width: 100%;">
                  <h6>INDUSTRY 4.0</h6>
                </div>
              </a>
            </div>
            <div class="col-md-2">
              <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-verticles.php#banking_services">
                <div class="icon_box">
                  <img src="assets/BFSI.png" class="img-fluid" style="width: 100%;">
                  <h6>BFSI</h6>
                </div>
              </a>
            </div>
            <div class="col-md-2">
              <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-verticles.php#original_equip_manf">
                <div class="icon_box">
                  <img src="assets/OEM.png" class="img-fluid" style="width: 100%;">
                  <h6>OEM</h6>
                </div>
              </a>
            </div>
            <div class="col-md-2">
              <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-verticles.php#energy_utilities">
                <div class="icon_box">
                  <img src="assets/EU.png" class="img-fluid" style="width: 100%;">
                  <h6>ENERGY & UTILITIES</h6>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>

      <!-- connecte_cars -->

      <div class="container spacing-md-md" id="connecte_cars">
        <div class="row">
          <div class="col-md-4 offset-md-4">
            <h2 class="text-center title-medium title-blue">CONNECTED CARS <div class="underline"></div></h2>
          </div>
          <div class="col-md-6">
            <h4 class="title-blue">About Connected Cars</h4>
            <p>Auto companies are increasingly turning to IoT and data science to explore insights for innovations such as self-driving vehicles, electric cars and mobility experiences</p>
          </div>
          <div class="col-md-6">
            <h4 class="title-blue">HODI Positioning</h4>
            <p>HODI provides end-to-end IoT solutions for automotive and related industries using sensors and hardware such as OBD, vehicle tracking, TCUs to capture, control and transmit data, manage connectivity, dash-boarding, reporting, alerting with basic to advanced analytics.s</p>
          </div>
        </div>

        <div class="container border_our_soln" id="our_solution">
        <div class="row">
          <div class="col-md-4 offset-md-4">
            <h5 class="text-center title-medium title-blue">Our Solutions</h5>
          </div>
        </div>

        <div class="row">
          <div class="col-md-3">
            <img src="assets/vertical/soln/cnnected_cars.png" class="img-fluid" alt="Header">
          </div>
          <div class="col-md-3">
            <img src="assets/vertical/soln/smart_driving.png" class="img-fluid" alt="Header">
          </div>
          <div class="col-md-3">
            <img src="assets/vertical/soln/cloud_based.png" class="img-fluid" alt="Header">
          </div>
          <div class="col-md-3">
            <img src="assets/vertical/soln/automated_emergency.png" class="img-fluid" alt="Header">
          </div>
        </div>

        <div class="row spacing-md">
          <div class="col-md-3">
            <img src="assets/vertical/soln/real_time.png" class="img-fluid" alt="Header">
          </div>
          <div class="col-md-3">
            <img src="assets/vertical/soln/driver_safety.png" class="img-fluid" alt="Header">
          </div>
          <div class="col-md-3">
            <img src="assets/vertical/soln/optimized_logistics.png" class="img-fluid" alt="Header">
          </div>
          <div class="col-md-3">
            <img src="assets/vertical/soln/prdictive_maintenance.png" class="img-fluid" alt="Header">
          </div>
        </div>
      </div>
      </div>

      

      <!-- transort_logistics -->
      <!-- <div class="container-fluid " id="transort_logistics">
        <div class="container">
          <div class="row spacing-md-md">
            <div class="col-md-6 offset-md-3">
              <h2 class="text-center title-medium title-blue">TRANSPORT & LOGISTICS<div class="underline"></div></h2>
            </div>
            <div class="col-md-6">
              <h4 class="title-blue">About Transport & Logistics</h4>
              <p>Transport & logistics management is broadly divided into infrastructure, vehicles and operations. IoT in this domain plays a significant role in maximizing supply chain efficiency in order to sustain profitability and viability</p>
            </div>
            <div class="col-md-6">
              <h4 class="title-blue">HODI Positioning</h4>
              <p>HODI provides end-to-end IoT solutions for Transportation and logistics industry using sensors and hardware such as RFID readers, OBD etc. to capture, control and transmit data, manage connectivity, dash-boarding, reporting, alerting with basic to advanced analytics.</p>
            </div>
          </div>

          <div class="container border_our_soln">
            <div class="row spacing-md">
              <div class="col-md-4 offset-md-4">
                <h5 class="text-center title-medium title-blue">Our Solutions</h5>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <img src="assets/vertical/soln/Track-and-Trace.png" class="img-fluid" alt="Header">
              </div>
              <div class="col-md-3">
                <img src="assets/vertical/soln/freight-control.png" class="img-fluid" alt="Header">
              </div>
              <div class="col-md-3">
                <img src="assets/vertical/soln/performance-measurement.png" class="img-fluid" alt="Header">
              </div>
              <div class="col-md-3">
                <img src="assets/vertical/soln/supplychain-optimization.png" class="img-fluid" alt="Header">
              </div>
            </div>

            <div class="row spacing-md">
              <div class="col-md-4 offset-md-4 text-center">
                <img src="assets/vertical/soln/warehoses-and-yard.png" class="img-fluid" alt="Header">
              </div>
            </div>
          </div>
        </div>
      </div> -->

      <!-- industry_04 -->
      <div class="container" id="industry_04">
        <div class="row spacing-md-md">
          <div class="col-md-6 offset-md-3">
            <h2 class="text-center title-medium title-blue">INDUSTRY 4.0<div class="underline"></div></h2>
          </div>
          <div class="col-md-6">
            <h4 class="title-blue">About Industry 4.0</h4>
            <p>Industry 4.0 is the new era of manufacturing intelligence, transforming manufacturing industry in the automation and data exchange space using modern technologies such as interconnected machines, connected devices, cyber physical systems, Cloud computing, big data and artificial intelligence. Industrial Internet of Things (IIoT) can be considered as a smart factory where whole physical process is monitored and controlled using a decentralized system.</p>
          </div>
          <div class="col-md-6">
            <h4 class="title-blue">HODI Positioning</h4>
            <p>HODI provides end-to-end IIoT solution across the manufacturing industry using sensors to capture data, actuators to control assets, gateways to transmit data, connectivity management, dash-boarding, reports and alerts and basic to advanced analytics.</p>
          </div>
        </div>

        <div class="container border_our_soln">
          <div class="row">
            <div class="col-md-4 offset-md-4">
              <h5 class="text-center title-medium title-blue">Our Solutions</h5>
            </div>
          </div>

          <div class="row text-center">
            <div class="col-md-4">
              <img src="assets/vertical/soln/procurement.png" class="img-fluid" alt="Header">
            </div>
            <div class="col-md-4">
              <img src="assets/vertical/soln/manufacturing.png" class="img-fluid" alt="Header">
            </div>
            <div class="col-md-4">
              <img src="assets/vertical/soln/distribution.png" class="img-fluid" alt="Header">
            </div>
          </div>
        </div>

      </div>

      <!-- banking_services -->
      <div class="container-fluid" id="banking_services">
        <div class="container">
          <div class="row spacing-md-md">
            <div class="col-md-6 offset-md-3">
              <h2 class="text-center title-medium title-blue">BANKING & FINANCIAL SERVICES<div class="underline"></div></h2>
            </div>
            <div class="col-md-6">
              <h4 class="title-blue">About BFSI</h4>
              <p>IoT is set to enable the financial services industry scale new heights via monitoring using sensors, controlling using actuators, displaying live data using dashboards, and generating insights from the huge data that is generated.</p>
            </div>
            <div class="col-md-6">
              <h4 class="title-blue">HODI Positioning</h4>
              <p>HODI provides IoT based FinTech solutions across the banking and financial services using sensors to capture data, actuators to control assets, gateways to transmit data, connectivity management, dash-boarding, reports and alerts and basic to advanced analytics.</p>
            </div>
          </div>

          <div class="container border_our_soln">
            <div class="row spacing-md">
              <div class="col-md-4 offset-md-4">
                <h5 class="text-center title-medium title-blue">Our Solutions</h5>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <img src="assets/vertical/soln/commercial_real_estate.png" class="img-responsive" style="width: 100%;" alt="Header">
              </div>
              <div class="col-md-3">
                <img src="assets/vertical/soln/Retail_Banking.png" class="img-fluid" style="width: 100%;" alt="Header">
              </div>
              <div class="col-md-3">
                <img src="assets/vertical/soln/commercial_banking.png" class="img-fluid" style="width: 100%;" alt="Header">
              </div>
              <div class="col-md-3">
                <img src="assets/vertical/soln/investment_managment.png" class="img-fluid" style="width: 100%;" alt="Header">
              </div>
            </div>

            <div class="row spacing-md">
              <div class="col-md-6 text-right">
                <img src="assets/vertical/soln/insurance.png" class="img-fluid"  alt="Header">
              </div>
              <div class="col-md-6 text-left">
                <img src="assets/vertical/soln/opertaion_optimize.png" class="img-fluid" alt="Header">
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <!-- original_equip_manf -->
      <div class="container-fluid" id="original_equip_manf">
        <div class="container">
          <div class="row spacing-md-md">
            <div class="col-md-8 offset-md-2 text-center">
              <h2 class="text-center title-medium title-blue">ORIGINAL EQUIPMENT MANUFACTURERS<div class="underline"></div></h2>
            </div>
            <div class="col-md-6">
              <h4 class="title-blue">About OEM</h4>
              <p>IoT enables OEMs in improving their operations efficiency and product quality. New insights from connected equipment can improve quality, inform service and engineering teams for better planning, offer insights into users’ behavior and generate new revenue streams.</p>
            </div>
            <div class="col-md-6">
              <h4 class="title-blue">HODI Positioning</h4>
              <p>HODI provides end-to-end IoT solutions for Transportation and logistics industry using sensors and hardware such as RFID readers, OBD etc. to capture, control and transmit data, manage connectivity, dash-boarding, reporting, alerting with basic to advanced analytics.</p>
            </div>
          </div>

          <div class="container border_our_soln">
            <div class="row">
              <div class="col-md-4 offset-md-4">
                <h5 class="text-center title-medium title-blue">Our Solutions</h5>
              </div>
            </div>

            <div class="row text-center">
              <div class="col-md-3">
                <img src="assets/vertical/soln/smart_driving-1.png" class="img-fluid" alt="Header">
              </div>
              <div class="col-md-3">
                <img src="assets/vertical/soln/Connectedproducts.png" class="img-fluid" alt="Header">
              </div>
              <div class="col-md-3">
                <img src="assets/vertical/soln/connectedsupplychain.png" class="img-fluid" alt="Header">
              </div>
              <div class="col-md-3">
                <img src="assets/vertical/soln/supportservice.png" class="img-fluid" alt="Header">
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="container-fluid" id="energy_utilities">
        <div class="container spacing-md-md">
          <div class="row">
            <div class="col-md-8 offset-md-2 text-center">
              <h2 class="text-center title-medium title-blue">ENERGY & UTILITIES<div class="underline"></div></h2>
            </div>
            <div class="col-md-6">
              <h4 class="title-blue">About Energy & Utilities</h4>
              <p>IoT will be playing a crucial role throughout the value chain of energy & utilities sector from power generation to long distance transmission to distribution to retailing to the end-consumers. According to Gartner the IoT usage in the energy sector is expected to grow to 50 billion devices by 2020.</p>
            </div>
            <div class="col-md-6">
              <h4 class="title-blue">HODI Positioning</h4>
              <p>HODI provides end-to-end IoT solution across the energy value chain using sensors to capture data, actuators to control assets, gateways to transmit data, connectivity management, dash-boarding, reports and alerts and basic to advanced analytics.</p>
            </div>
          </div>

          <div class="container border_our_soln">
            <div class="row">
              <div class="col-md-4 offset-md-4">
                <h5 class="text-center title-medium title-blue">Our Solutions</h5>
              </div>
            </div>

            <div class="row text-center">
              <div class="col-md-4">
                <img src="assets/vertical/soln/Generation.png" class="img-fluid" alt="Header">
              </div>
              <div class="col-md-4">
                <img src="assets/vertical/soln/transmission.png" class="img-fluid" alt="Header">
              </div>
              <div class="col-md-4">
                <img src="assets/vertical/soln/retail.png" class="img-fluid" alt="Header">
              </div>
            </div>
          </div>

        </div>
      </div>

    <?php include('templates/partials/footer.php'); ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script> -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="OwlCarousel2-2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="main.js"></script>
    
  </body>
</html>