<nav class="navbar sticky-top navbar-expand-lg navbar-dark" id="mymenu">
  <div class="container">
    <a class="navbar-brand" href="index.php">
      <img src="assets/logo_unlimit_new2.png" class="d-inline-block align-top img-fluid" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse " id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="index.php">HOME <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle text-center" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            SOLUTIONS
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-control.php">DEVICE</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-enablement.php">PLATFORM</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-insights.php">IOTA</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-verticles.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            VERTICALS
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="hodi-verticles.php#connecte_cars">CONNECTED CARS</a>
            <div class="dropdown-divider"></div>
            <!-- <a class="dropdown-item" href="hodi-verticles.php#transort_logistics">TRANSPORT & LOGISTICS</a>
            <div class="dropdown-divider"></div> -->
            <a class="dropdown-item" href="hodi-verticles.php#industry_04">INDUSTRY 4.0</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="hodi-verticles.php#banking_services">AGRICULTURE</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="hodi-verticles.php#original_equip_manf">OEM</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="hodi-verticles.php#energy_utilities">ENERGY & UTILITIES</a>
          </div>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="hodi-partners.php">PARTNERS</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="hodi-use-cases.php">USE CASES</a>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="hodi-about-us.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            ABOUT US
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="hodi-about-us.php#our_team">OUR TEAM</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="hodi-about-us.php#careers">Careers</a>
          </div>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="hodi-contact-us.php">CONTACT US</a>
        </li>

      </ul>
    </div>
  </div>
</nav>