<footer id="footer">
  <div class="container-fluid">
    <div class="row">

      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <h6>SOLUTION</h6>
            <ul class="list-inline">
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-control.php">Device</a></li>
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-enablement.php">Platform</a></li>
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-insights.php">IOAT</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <h6>VERTICALS</h6>
            <ul class="list-inline">
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-verticles.php#connecte_cars">Connected Cars</a></li>
              <!-- <li class="list-inline"><a href="< ?php echo dirname($_SERVER['PHP_SELF']); ? >/hodi-verticles.php#transort_logistics">Transport & Logistics</a></li> -->
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-verticles.php#industry_04">Industrial OEM & BESPOKE</a></li>
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-verticles.php#banking_services">BFSI</a></li>
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-verticles.php#original_equip_manf">OEM</a></li>
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-verticles.php#energy_utilities">Energy & Utilities</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <h6>PARTNERS</h6>
            <ul class="list-inline">
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-partners.php">HODI Partners</a></li>
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-partners.php#featuredpartner">Partner</a></li>
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-partners.php#join_us">Become a Partner</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <h6>ABOUT US</h6>
            <ul class="list-inline">
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-about-us.php">About HODI</a></li>
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-about-us.php#our_team">Team HODI</a></li>
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-about-us.php#careers">Careers</a></li>
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-use-cases.php">Use Cases</a></li>
              <li class="list-inline"><a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-contact-us.php">Contact Us</a></li>
            </ul>
          </div>
        </div> 
      </div>

      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
      
      <div class="container sub-footer">
        <div class="row">
          <div class="col-md-8">
            <span class="copyright">Copyright © HODI 2018. Powered by Alandick & Co East Africa Limited</span>
          </div>
          <div class="col-md-4">
            <div class="social_icons float-right">
                <i class="fa fa-facebook" aria-hidden="true"></i>
                <i class="fa fa-twitter" aria-hidden="true"></i>
                <i class="fa fa-google-plus" aria-hidden="true"></i>
                <i class="fa fa-youtube" aria-hidden="true"></i>
                <i class="fa fa-linkedin" aria-hidden="true"></i>
              </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</footer>