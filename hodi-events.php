<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.carousel.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title>HODI | Events</title>
  </head>

    <body>

      <div class="suspended_icon">
        <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-contact-us.php">
          <img src="assets/for_a_demo.jpg" width="100%" class="img-fluid">
        </a>
      </div>

    <?php include('templates/partials/nav.php'); ?>

      <div class="container-fluid bg_header_img verticle">
        <div class="row">

          <div class="container ">
            <div class="row">
              <div class="col-md-8 correct_margin ">
                <div class="">
                    <h2 class="title-semi-big-slider title-small-top">Commercial Vehicle Forum 2018</h2>
                    <a href="#" class="btn btn-primary">EXPLORE THE EVENT</a><!-- hodi-explore-events.php -->
                </div>
              </div>
              <div class="col-md-4">
                
              </div>
            </div>
          </div>
          
        </div>
      </div>


<div class="container mt-3 spacing-bg " id="events">
  <ul class="nav nav-tabs nav-justified col-md-4 offset-md-4">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#home">All</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu1">Upcoming</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu2">2018</a>
    </li>
  </ul>

  <div class="col-md-12">
    <hr/>
  </div>

  <!-- Tab panes -->
  <div class="tab-content">
    <div id="home" class="container tab-pane active"><br>
      <div class="row">
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Infrastructure Innovation summit 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Global Summit on "Data Protection, Privacy & Security"</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Commercial Vehicle Forum 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
            <button type="submit" class="btn btn-primary">EXPLORE THE EVENT</button>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">MWC Barcelona 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
            <button type="submit" class="btn btn-primary">EXPLORE THE EVENT</button>
          </div>
        </div>
      </div>
      <div class="row spacing-md">
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Global Summit on "Data Protection, Privacy & Security"</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Infrastructure Innovation summit 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
            <button type="submit" class="btn btn-primary">EXPLORE THE EVENT</button>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">MWC Barcelona 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Commercial Vehicle Forum 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div id="menu1" class="container tab-pane fade"><br>
      <div class="row">
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Infrastructure Innovation summit 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Global Summit on "Data Protection, Privacy & Security"</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Commercial Vehicle Forum 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
            <button type="submit" class="btn btn-primary">EXPLORE THE EVENT</button>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">MWC Barcelona 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
            <button type="submit" class="btn btn-primary">EXPLORE THE EVENT</button>
          </div>
        </div>
      </div>
      <div class="row spacing-md">
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Global Summit on "Data Protection, Privacy & Security"</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Infrastructure Innovation summit 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
            <button type="submit" class="btn btn-primary">EXPLORE THE EVENT</button>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">MWC Barcelona 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Commercial Vehicle Forum 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div id="menu2" class="container tab-pane fade"><br>
      <div class="row spacing-md">
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Global Summit on "Data Protection, Privacy & Security"</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Infrastructure Innovation summit 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
            <button type="submit" class="btn btn-primary">EXPLORE THE EVENT</button>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">MWC Barcelona 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Commercial Vehicle Forum 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Infrastructure Innovation summit 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Global Summit on "Data Protection, Privacy & Security"</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">Commercial Vehicle Forum 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
            <button type="submit" class="btn btn-primary">EXPLORE THE EVENT</button>
          </div>
        </div>
        <div class="col-md-3">
          <div class="blog_box">
            <h5 class="title-blue">MWC Barcelona 2018</h5>
            <ul class="list-unstyled">
              <li>Start Date: 23 May 2018</li>
              <li>End Date: 24 May 2018</li>
              <li>Venue: Mumbai</li>
            </ul>
            <button type="submit" class="btn btn-primary">EXPLORE THE EVENT</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>


  <?php include('templates/partials/footer.php'); ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script> -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="OwlCarousel2-2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="main.js"></script>
    
  </body>
</html>