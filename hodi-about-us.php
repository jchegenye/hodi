  <!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.carousel.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title>HODI | About Us</title>
  </head>

    <body>

      <div class="suspended_icon">
        <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-contact-us.php">
          <img src="assets/for_a_demo.jpg" width="100%" class="img-fluid">
        </a>
      </div>

    <?php include('templates/partials/nav.php'); ?>

    <div class="container-fluid bg_header_img verticle">
      <div class="row">

        <div class="container ">
          <div class="row">
            <div class="col-md-8 correct_margin ">
              <div class="">
                  <h2 class="title-semi-big-slider title-small-top">We Are Fast Forwarding The Future To You!</h2>
                  <p>Set Up By The Reliance Group, We Have Pioneered As a Dedicated IoT Business Unit.</p>
              </div>
            </div>
            <div class="col-md-4">
              
            </div>
          </div>
        </div>
        
      </div>
    </div>

    <div class="container-fluid spacing-bg" id="verticles">
      <div class="container">
        <div class="row">
          <div class="col-md-4 offset-md-4">
            <h2 class="text-center title-medium">ABOUT US<div class="underline"></div></h2>
           </div>
        </div>

        <div class="row">
          <div class="col-md-12 text-left">
            <p>In developing economies like India, digital transformation has a strong impact on future success. One of the key success factors come from the Internet of Things (IoT) en route towards enabling digital transformation in all main areas of the economy. To support this mega trend, Reliance Group has founded Unlimit as an independent unit to develop and support IoT in best possible way for the Indian market. We have established our mark in the Indian corporate landscape as a business unit that solely focuses on IoT and the Enterprise (B-2-B-2-x) model.</p>

            <P>
              Backed by India’s leading conglomerate, Unlimit is driven by an in-depth knowledge of the Indian market and an unparalleled expertise of IoT implementations ranging from Connectivity to Device Management, Application Softwares and any form of Analytics. We strive to add value to our products and services through strategic partnerships with leading established IoT stakeholders such as Cisco Jasper, Cumulocity – A Software AG Company and IBM Watson IoT. Our ever-growing partnerships with global IoT powerhouses enable both Indian and foreign companies to grow their respective businesses by getting to market faster and scaling quickly with high-service reliability at low ownership costs.
            </P>
          </div>
        </div>

      </div>
    </div>

    <div class="container spacing-bg">
      <div class="row vertical-align">
        <div class="col-md-4 offset-md-4">
          <h2 class="text-center title-medium title-blue">PARTNER CATALOGUE<div class="underline"></div></h2>
        </div>
      </div>

      <div class="row title-medium">
        <div class="col-md-4">
          <img src="assets/ourteam/our_vision.png" class="img-fluid mx-auto d-block" alt="Header">
          <h4 class="text-center title-medium spacing-md">OUR VISION</h4>
          <p>At Unlimit, we strive to become the leading and most innovative IoT company in India. Let us drive your IoT Business.</p>
        </div>
        <div class="col-md-4">
          <img src="assets/ourteam/our_mission.png" class="img-fluid mx-auto d-block" alt="Header">
          <h4 class="text-center title-medium spacing-md">OUR VISION</h4>
          <p>To digitally transform our customers’ business models and drive value by offering a comprehensive IoT platform from Connectivity to Application Development and Analytics. We are the end-to-end provider of your all IoT related needs.</p>
        </div>
        <div class="col-md-4">
          <img src="assets/ourteam/our_values.png" class="img-fluid mx-auto d-block" alt="Header">
          <h4 class="text-center title-medium spacing-md">OUR VISION</h4>
          <p>Innovation: Be innovative every day, in every way! Entrepreneurship: Take responsibilities beyond our comfort zone! Action: Take actions at the right time, in the right way!</p>
        </div>
      </div>

    </div>

    <div class="container-fluid make_bg_gray" id="our_team">
      <div class="container">
        <div class="row vertical-align">
          <div class="col-md-4 offset-md-4 text-center">
            <h2 class="text-center title-medium title-blue">OUR TEAM<div class="underline"></div></h2>
          </div>
          <div class="col-md-12">
            <p class="text-center">We’ve assembled a unique team of the brightest technologists, engineers, and business consultants.</p>
          </div>
        </div>

        <div class="row spacing-md">
          <div class="col-md-3">
            <img src="assets/default_bio_bg.png" class="img-fluid mx-auto d-block" alt="Header">
            <h4 class="text-center spacing-md text-center">John Doe</h4>
            <h6 class="text-center spacing-md-bottom">Chief Executive Office (CEO)</h6>
            <button type="submit" class="btn btn-primary mx-auto d-block">READ MORE</button>
          </div>
          <div class="col-md-3">
            <img src="assets/default_bio_bg.png" class="img-fluid mx-auto d-block" alt="Header">
            <h4 class="text-center spacing-md text-center">John Doe</h4>
            <h6 class="text-center spacing-md-bottom">Chief Executive Office (CEO)</h6>
            <button type="submit" class="btn btn-primary mx-auto d-block">READ MORE</button>
          </div>
          <div class="col-md-3">
            <img src="assets/default_bio_bg.png" class="img-fluid mx-auto d-block" alt="Header">
            <h4 class="text-center spacing-md text-center">John Doe</h4>
            <h6 class="text-center spacing-md-bottom">Chief Executive Office (CEO)</h6>
            <button type="submit" class="btn btn-primary mx-auto d-block">READ MORE</button>
          </div>
          <div class="col-md-3">
            <img src="assets/default_bio_bg.png" class="img-fluid mx-auto d-block" alt="Header">
            <h4 class="text-center spacing-md text-center">John Doe</h4>
            <h6 class="text-center spacing-md-bottom">Chief Executive Office (CEO)</h6>
            <button type="submit" class="btn btn-primary mx-auto d-block">READ MORE</button>
          </div>
        </div>
      </div>
    </div>

    <div class="container spacing-bg" id="careers">
      <div class="row vertical-align">
        <div class="col-md-4 offset-md-4 text-center">
          <h2 class="text-center title-medium title-blue">APPLY NOW<div class="underline"></div></h2>
        </div>
        <div class="col-md-12">
          <p class="text-center col-md-8 offset-md-2">Working with Unlimit is more than just a job – it’s an opportunity to be a part of something bigger. Join the Unlimit Team by filling out the application below.</p>
        </div>
      </div>
      
      <div class=" spacing-md">
        <form>

          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="inputEmail4">First Name:*</label>
              <input type="email" class="form-control" id="inputEmail4">
            </div>
            <div class="form-group col-md-4">
              <label for="inputEmail4">Last Name:*</label>
              <input type="email" class="form-control" id="inputEmail4">
            </div>
            <div class="form-group col-md-4">
              <label for="inputEmail4">E-mail:*</label>
              <input type="email" class="form-control" id="inputEmail4">
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="inputEmail4">Phone:*</label>
              <input type="email" class="form-control" id="inputEmail4">
            </div>
            <div class="form-group col-md-4">
              <label for="inputEmail4">Linked URL:</label>
              <input type="email" class="form-control" id="inputEmail4">
            </div>
            <div class="form-group col-md-4">
              <label for="inputEmail4">Experience:*</label>
              <div class="row">
                <div class="col-md-6">
                  <input type="email" class="form-control" id="inputEmail4" placeholder="Years"> 
                </div>
                <div class="col-md-6">
                  <input type="email" class="form-control" id="inputEmail4" placeholder="Months">
                </div>
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="inputEmail4">Notice Period:*</label>
              <input type="email" class="form-control" id="inputEmail4">
            </div>
            <div class="form-group col-md-4">
              <label for="inputEmail4">Current CTC(LPA):</label>
              <input type="email" class="form-control" id="inputEmail4">
            </div>
            <div class="form-group col-md-4">
              <label for="inputEmail4">Expected CTC(LPA):</label>
              <input type="email" class="form-control" id="inputEmail4">
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="inputEmail4">Current City:*</label>
              <input type="email" class="form-control" id="inputEmail4">
            </div>
            <div class="form-group col-md-4">
              <label for="inputEmail4">Current Company:*</label>
              <input type="email" class="form-control" id="inputEmail4">
            </div>
            <div class="form-group col-md-4">
              <label for="inputEmail4">Attach CV:* <small class="">(only .doc, .docx, .rtf, and .pdf)</small></label>
              <input type="file" class="form-control-file" id="exampleFormControlFile1">
            </div>
          </div>

          <button type="submit" class="btn btn-primary">Submit</button>

        </form>
      </div>

      </div>
    </div>

    <?php include('templates/partials/footer.php'); ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script> -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="OwlCarousel2-2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="main.js"></script>
    
  </body>
</html>