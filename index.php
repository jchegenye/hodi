<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.carousel.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title>HODI</title>
  </head>

    <body>

      <div class="suspended_icon">
        <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-contact-us.php">
          <img src="assets/for_a_demo.jpg" width="100%" class="img-fluid">
        </a>
      </div>

    <?php include('templates/partials/nav.php'); ?>

    <div class="owl-carousel owl-theme owl-one">
      <div class="item">
        <img src="assets/smartfarm-1.jpg" class="img-fluid" alt="Responsive image" style="width: 100%;">
        
        <div class="boxi">
          <div class="container ">
            <div class="row">
              <div class="col-md-8">
                <h3 class="title-blue">WITH HODI</h3>
                <h3 class="title-semi-big-slider">YOU WILL SAVE MONEY FROM DAY ONE</h3>
                <p>Our solutions and products are fully tailored to your needs to ensure maximum productivity coupled with the highest customer benefit</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <img src="assets/smartfarm-2.jpg" class="img-fluid" alt="Responsive image" style="width: 100%;">
      </div>
      <div class="item"><img src="assets/smartfarm-3.jpg" class="img-fluid" alt="Responsive image" style="width: 100%;"></div>
    </div>

   <!--  <div class="container-fluid latest_news">
      <div class="row">

        <div class="container">
          <div class="row">
            <div class="col-md-2">
              <h5 class="title-blue">LATEST NEWS</h5>
            </div>
            <div class="col-md-1">
              <div class="horizontal_line text-center">|</div>
            </div>
            <div class="col-md-7">
              <p>Global Summit on "Data Protection, Privacy & Security", 11th May 2018, New Delhi</p>
            </div>
          </div>
        </div>

      </div>
    </div> -->

    <div class="container-fluid spacing-bg" id="verticles">
      <div class="container">
        <div class="row">
          <div class="col-md-4 offset-md-4">
            <h2 class="text-center title-medium">VERTICALS <div class="underline"></div></h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-2">
            <div class="icon_box">
              <img src="assets/Automotive.png" class="img-fluid" style="width: 100%;">
              <h6>CONNECTED CARS</h6>
            </div>
          </div>
          <!-- <div class="col-md-2">
            <div class="icon_box">
              <img src="assets/TL.png" class="img-fluid" style="width: 100%;">
              <h6>TRANSPORT & LOGISTICS</h6>
            </div>
          </div> -->
          <div class="col-md-2">
            <div class="icon_box">
              <img src="assets/Industry.png" class="img-fluid" style="width: 100%;">
              <h6>INDUSTRIALS OEM & BESPOKE</h6>
            </div>
          </div>
          <div class="col-md-2">
            <div class="icon_box">
              <img src="assets/BFSI.png" class="img-fluid" style="width: 100%;">
              <h6>BFSI</h6>
            </div>
          </div>
          <div class="col-md-2">
            <div class="icon_box">
              <img src="assets/OEM.png" class="img-fluid" style="width: 100%;">
              <h6>OEM</h6>
            </div>
          </div>
          <div class="col-md-2">
            <div class="icon_box">
              <img src="assets/EU.png" class="img-fluid" style="width: 100%;">
              <h6>ENERGY & UTILITIES</h6>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container spacing-bg">
      <div class="row">
        <div class="col-md-4 offset-md-4">
          <h2 class="text-center title-medium">END TO END SOLUTIONS <div class="underline"></div></h2>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4">
          <!-- <img src="assets/Unlimit-Control_new.gif.gif" class="img-fluid" style="width: 100%;"> -->
        </div>

        <div class="col-md-8 emphasize-center">
          <h3 class="title-blue">DEVICE</h3>

            <h6>Manage connections of all your IoT devices.</h6>
            <p>Ensure your devices connect reliably, securily, and cost-efficiently with device, the IoT platform that Automotiveates connections management.</p>

            <button class="btn btn-primary">KNOW MORE</button>
        </div>
      </div>

      <div class="row">
        <div class="col-md-8 emphasize-center">
          <h3 class="title-blue">PLATFORM</h3>

            <h6>Build, develop and deploy smart connected solutions.</h6>
            <p>Unlock the value of your business with the help of our fully tailored device and application management platform.</p>

            <button class="btn btn-primary">KNOW MORE</button>
        </div>

        <div class="col-md-4">
          <!-- <img src="assets/Unlimit-Enablement.gif.gif" class="img-fluid" style="width: 100%;"> -->
        </div>
      </div>

      <div class="row">
        <div class="col-md-4">
          <!-- <img src="assets/Unlimit-Insights.gif" class="img-fluid" style="width: 100%;"> -->
        </div>

        <div class="col-md-8 emphasize-center">
          <h3 class="title-blue">IOTA</h3>

            <h6>Validate, forecast & optimize your business.</h6>
            <p>Get meaningful insights from real-time data and explore the potential of Machine Learning, Cognitive Computing & Artificial Intelligence.</p>

            <button class="btn btn-primary">KNOW MORE</button>
        </div>
      </div>
    </div>

    <div class="container-fluid spacing-bg" id="partner-eco">
      <div class="container">
        <div class="row">
          <div class="col-md-4 offset-md-4">
            <h2 class="text-center">PARTNERS <div class="underline"></div></h2>
            <h6 class="text-center title-medium">Regular Partners</h6>
          </div>
        </div>

        <div class="row">
          <div class="col-md-4">
            <img src="assets/strategic-Partnwer_new.png" class="img-fluid" style="width: 100%;">
          </div>

          <div class="col-md-8">
            <img src="assets/Allied-partners_new.png" class="img-fluid" style="width: 100%; height: 458px;">
          </div>
        </div>

        <div class="row">
          <div class="col-md-4 offset-md-4">
            <p style="margin-top: 30px;">JOIN OUR PARTNER > ></p>
          </div>
          
        </div>
      </div>
    </div>

    <?php include('templates/partials/footer.php'); ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script> -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="OwlCarousel2-2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="main.js"></script>
    
  </body>
</html>