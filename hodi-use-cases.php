<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.carousel.min.css">
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title>HODI | Use Cases</title>
  </head>

    <body>

      <div class="suspended_icon">
        <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>/hodi-contact-us.php">
          <img src="assets/for_a_demo.jpg" width="100%" class="img-fluid">
        </a>
      </div>

    <?php include('templates/partials/nav.php'); ?>

      <div class="container-fluid bg_header_img verticle">
        <div class="row">

          <div class="container ">
            <div class="row">
              <div class="col-md-8 correct_margin ">
                <div class="">
                    <h2 class="title-semi-big-slider title-small-top">Be Amazed With The Limitless Possibilities</h2>
                </div>
              </div>
              <div class="col-md-4">
                
              </div>
            </div>
          </div>
          
        </div>
      </div>

      <div class="container-fluid spacing-bg" id="verticles">
        <div class="container v_mainpage">
          <div class="row">
            <div class="col-md-4 offset-md-4">
              <h2 class="text-center title-medium">VERTICALS <div class="underline"></div></h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2">
              <div class="icon_box">
                <img src="assets/Automotive.png" class="img-fluid" style="width: 100%;">
                <h6>CONNECTED CARS</h6>
              </div>
            </div>
            <div class="col-md-2">
              <div class="icon_box">
                <img src="assets/TL.png" class="img-fluid" style="width: 100%;">
                <h6>TRANSPORT & LOGISTICS</h6>
              </div>
            </div>
            <div class="col-md-2">
              <div class="icon_box">
                <img src="assets/Industry.png" class="img-fluid" style="width: 100%;">
                <h6>INDUSTRY 4.0</h6>
              </div>
            </div>
            <div class="col-md-2">
              <div class="icon_box">
                <img src="assets/BFSI.png" class="img-fluid" style="width: 100%;">
                <h6>BFSI</h6>
              </div>
            </div>
            <div class="col-md-2">
              <div class="icon_box">
                <img src="assets/OEM.png" class="img-fluid" style="width: 100%;">
                <h6>OEM</h6>
              </div>
            </div>
            <div class="col-md-2">
              <div class="icon_box">
                <img src="assets/EU.png" class="img-fluid" style="width: 100%;">
                <h6>ENERGY & UTILITIES</h6>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 offset-md-5 spacing-md">
              <div class="icon_box">
                <img src="assets/vertical/icons/miles.png" class="img-fluid" style="width: 100%;">
                <h6>MISCELLANEOUS</h6>
              </div>
            </div>
          </div>
        </div>
      </div>


    <?php include('templates/partials/footer.php'); ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script> -->
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="OwlCarousel2-2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="main.js"></script>
    
  </body>
</html>